package main

import (
	"enter-scraper/config"
	"enter-scraper/controller"
	"fmt"
)

var (
	setConfigLog = "Set %v config: %v"
)

func main() {
	logConf := config.NewLoggerConfig()
	log := config.NewLogger(logConf)

	aggregConf := config.NewAggregatorConfig()
	log.Debug(fmt.Sprintf(setConfigLog, "aggregation", aggregConf))

	scrapeConf := config.NewSensorsConfig()
	log.Debug(fmt.Sprintf(setConfigLog, "scrape", scrapeConf))

	ctrl := controller.NewController(log, aggregConf, scrapeConf)

	if err := ctrl.Control(); err != nil {
		log.Fatal(fmt.Sprintf("Control error: %v", err))
	}
}
