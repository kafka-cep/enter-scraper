package sender

import (
	"enter-scraper/config"
	"go.uber.org/zap"
)

type Sender interface {
	Send([]byte) error
}

func SetSender(name string, logger *zap.Logger, config config.AggregatorConfig) (Sender, error) {
	switch name {
	default:
		return NewAggregatorSender(logger, config)
	}
}
