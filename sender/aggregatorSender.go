package sender

import (
	"bytes"
	"enter-scraper/config"
	"fmt"
	"go.uber.org/zap"
	"io"
	"net/http"
	"net/url"
)

type AggregatorSender struct {
	logger *zap.Logger

	url *url.URL
}

func NewAggregatorSender(logger *zap.Logger, conf config.AggregatorConfig) (*AggregatorSender, error) {
	res := &AggregatorSender{
		logger: logger,
		url:    nil,
	}

	_url, err := url.Parse(conf.URL)
	if err != nil {
		return nil, err
	}

	res.url = _url

	return res, nil
}

// TODO: Пределать в нормальный вид
func (ag *AggregatorSender) Send(data []byte) error {
	resp, err := http.Post(ag.url.String(), "application/json", bytes.NewReader(data))
	if err != nil || resp == nil {
		ag.logger.Error(fmt.Sprintf("Cannot send to aggregator: %v", err))
		return err
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		ag.logger.Error(fmt.Sprintf("Cannot read response body from aggregator: %v", err))
		return err
	}

	if resp.StatusCode >= 300 && resp.StatusCode < 200 {
		ag.logger.Error(fmt.Sprintf("Query not succsess: status code %v, body: %v", resp.StatusCode, string(body)))
		return nil
	}

	ag.logger.Info(fmt.Sprintf("Aggregate return response: %v", body))
	return nil
}
