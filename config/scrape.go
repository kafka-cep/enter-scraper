package config

import (
	"bufio"
	"os"
	"strconv"
)

type ScrapeConfig struct {
	IPs []string
	API string
	Gen int
}

// TODO: Fix panic
func NewSensorsConfig() ScrapeConfig {
	sn := ScrapeConfig{
		IPs: []string{"localhost:9200"},
		API: "/scrape",
		Gen: 0,
	}

	if val, ok := os.LookupEnv("SCRAPE_IPS_PATH"); ok {
		var res []string

		fl, err := os.Open(val)
		if err != nil {
			panic(err)
		}
		defer fl.Close()

		scanner := bufio.NewScanner(fl)
		for scanner.Scan() {
			res = append(res, scanner.Text())
		}
		if err = scanner.Err(); err != nil {
			panic(err)
		}

		sn.IPs = res
	}

	if val, ok := os.LookupEnv("SCRAPE_API"); ok {
		sn.API = val
	}

	if val, ok := os.LookupEnv("SCRAPE_GEN"); ok {
		var err error
		sn.Gen, err = strconv.Atoi(val)
		if err != nil {
			sn.Gen = 0
		}
	}

	return sn
}
