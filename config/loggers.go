package config

import (
	"fmt"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
)

type LoggerConfig struct {
	Level  string
	Format string
}

func NewLoggerConfig() LoggerConfig {
	conf := LoggerConfig{
		Level:  "info",
		Format: "console",
	}

	if val, ok := os.LookupEnv("LOG_LEVEL"); ok {
		conf.Level = val
	}

	if val, ok := os.LookupEnv("LOG_FORMAT"); ok {
		conf.Format = val
	}

	return conf
}

func NewLogger(conf LoggerConfig) *zap.Logger {
	config := zap.Config{
		Level:             level(conf),
		Development:       false,
		Encoding:          conf.Format,
		DisableCaller:     false,
		DisableStacktrace: false,
		EncoderConfig: zapcore.EncoderConfig{
			MessageKey:    "massage",
			LevelKey:      "level",
			TimeKey:       "timestamp",
			NameKey:       "service",
			StacktraceKey: "stacktrace",
			EncodeTime:    zapcore.RFC3339TimeEncoder,
		}, OutputPaths: []string{
			"stdout",
		},
		ErrorOutputPaths: []string{
			"stderr",
		},
		InitialFields: map[string]interface{}{
			"pid": os.Getpid(),
		},
	}

	log := zap.Must(config.Build())
	log.Debug(fmt.Sprintf("Set logger with config: %v", config))

	return log
}

func level(conf LoggerConfig) zap.AtomicLevel {
	switch conf.Level {
	case "error":
		return zap.NewAtomicLevelAt(zap.ErrorLevel)
	case "warn":
		return zap.NewAtomicLevelAt(zap.InfoLevel)
	case "debug":
		return zap.NewAtomicLevelAt(zap.DebugLevel)
	default:
		return zap.NewAtomicLevelAt(zap.InfoLevel)
	}
}
