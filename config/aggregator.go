package config

import "os"

type AggregatorConfig struct {
	URL string
}

func NewAggregatorConfig() AggregatorConfig {
	ag := AggregatorConfig{
		URL: "localhost:8080/aggregate",
	}

	if val, ok := os.LookupEnv("AGGREGATE_URL"); ok {
		ag.URL = val
	}

	return ag
}

func (ag AggregatorConfig) Config() map[string]string {
	res := make(map[string]string)
	res["url"] = ag.URL
	return res
}
