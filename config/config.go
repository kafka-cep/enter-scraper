package config

type Config interface {
	Config() map[string]string
}
