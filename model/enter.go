package model

import (
	"time"
)

type Enter struct {
	TS       time.Time
	ID       int
	Status   bool
	LastOpen time.Time
	Type     string
}

func (en *Enter) Open() {
	eventTime := time.Now()
	en.LastOpen = eventTime
	en.TS = eventTime
	en.Status = true
}

func (en *Enter) Close() {
	en.TS = time.Now()
	en.Status = false
}
