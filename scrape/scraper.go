package scrape

import (
	"enter-scraper/config"
	"fmt"
	"go.uber.org/zap"
	"strings"
)

type Scraper interface {
	Scrape() ([]byte, error)
}

/*func ScrapeFactoryMethod(logger zap.Logger, config config.ScrapeConfig) Scraper {
	if config.Gen > 0 {
		return seeds.NewGenScrape(logger, config.Gen)
	} else {
		return NewSensorScrape(logger, config)
	}
}*/

// TODO: Хардкод схемы http
// TODO: Сменить передачу конфига по значению на передачу по ссылке
func ScrapeFill(logger *zap.Logger, conf config.ScrapeConfig) []Scraper {
	logger.Debug("Start filling scrapes...")

	scrapes := make([]Scraper, len(conf.IPs))

	for i, val := range conf.IPs {
		build := strings.Builder{}
		build.WriteString("http://")
		build.WriteString(val)
		build.WriteString(conf.API)

		scrape, err := NewSensorScrape(logger, build.String())
		//TODO: Может быть скрытое поведение
		if err != nil {
			logger.Warn(fmt.Sprintf("Not valid scrape url: %v. Err: %v", build.String(), err))
			continue
		}
		scrapes[i] = scrape
	}

	return scrapes
}
