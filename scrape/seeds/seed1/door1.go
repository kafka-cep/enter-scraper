package seed1

import (
	"encoding/json"
	"enter-scraper/model"
	"time"
)

type genDoor1 struct {
	start time.Time
	data  *model.Enter
}

func NewGenDoor1() *genDoor1 {
	return &genDoor1{
		start: time.Now(),
		data: &model.Enter{
			TS:       time.Now(),
			ID:       1,
			Status:   false,
			LastOpen: time.Now().Add(-time.Hour * 2),
			Type:     "door",
		},
	}
}

// TODO: При конкуррентной модели дублируется открытие так как меньше 2 с
func (gn *genDoor1) Scrape() ([]byte, error) {
	if sc := time.Since(gn.start); sc >= time.Second*1 && sc < time.Second*2 || sc >= time.Second*1830 && sc < time.Second*1832 {
		gn.data.Open()
		return json.Marshal(gn.data)
	}
	gn.data.Close()
	return json.Marshal(gn.data)
}
