package seed1

import (
	"encoding/json"
	"enter-scraper/model"
	"time"
)

type genDoor5 struct {
	start time.Time
	data  *model.Enter
}

func NewGenDoor5() *genDoor5 {
	return &genDoor5{
		start: time.Now(),
		data: &model.Enter{
			TS:       time.Now(),
			ID:       5,
			Status:   false,
			LastOpen: time.Now().Add(-time.Hour * 2),
			Type:     "door",
		},
	}
}

func (gn *genDoor5) Scrape() ([]byte, error) {
	if sc := time.Since(gn.start); sc >= time.Second*20 && sc < time.Second*21 || sc >= time.Second*680 && sc < time.Second*681 {
		gn.data.Open()
		return json.Marshal(gn.data)
	}
	gn.data.Close()
	return json.Marshal(gn.data)
}
