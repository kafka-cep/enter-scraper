package seed1

import (
	"encoding/json"
	"enter-scraper/model"
	"time"
)

type genDoor15 struct {
	start time.Time
	data  *model.Enter
}

func NewGenDoor15() *genDoor15 {
	return &genDoor15{
		start: time.Now(),
		data: &model.Enter{
			TS:       time.Now(),
			ID:       15,
			Status:   false,
			LastOpen: time.Now().Add(-time.Hour * 2),
			Type:     "door",
		},
	}
}

func (gn *genDoor15) Scrape() ([]byte, error) {
	if sc := time.Since(gn.start); sc >= time.Second*800 && sc < time.Second*801 || sc >= time.Second*980 && sc < time.Second*981 {
		gn.data.Open()
		return json.Marshal(gn.data)
	}
	gn.data.Close()
	return json.Marshal(gn.data)
}
