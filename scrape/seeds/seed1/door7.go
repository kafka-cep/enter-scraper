package seed1

import (
	"encoding/json"
	"enter-scraper/model"
	"time"
)

type genDoor7 struct {
	start time.Time
	data  *model.Enter
}

func NewGenDoor7() *genDoor7 {
	return &genDoor7{
		start: time.Now(),
		data: &model.Enter{
			TS:       time.Now(),
			ID:       7,
			Status:   false,
			LastOpen: time.Now().Add(-time.Hour * 2),
			Type:     "door",
		},
	}
}

func (gn *genDoor7) Scrape() ([]byte, error) {
	if sc := time.Since(gn.start); sc >= time.Second*10 && sc < time.Second*11 {
		gn.data.Open()
		return json.Marshal(gn.data)
	}
	gn.data.Close()
	return json.Marshal(gn.data)
}
