package seed1

import (
	"encoding/json"
	"enter-scraper/model"
	"time"
)

type genDoor3 struct {
	start time.Time
	data  *model.Enter
}

func NewGenDoor3() *genDoor3 {
	return &genDoor3{
		start: time.Now(),
		data: &model.Enter{
			TS:       time.Now(),
			ID:       3,
			Status:   false,
			LastOpen: time.Now().Add(-time.Hour * 2),
			Type:     "door",
		},
	}
}

func (gn *genDoor3) Scrape() ([]byte, error) {
	if sc := time.Since(gn.start); sc > time.Second*4 && sc < time.Second*5 || sc >= time.Second*1800 && sc < time.Second*1802 {
		gn.data.Open()
		return json.Marshal(gn.data)
	}
	gn.data.Close()
	return json.Marshal(gn.data)
}
