package seed1

import (
	"encoding/json"
	"enter-scraper/model"
	"time"
)

type genOther struct {
	start time.Time
	data  *model.Enter
}

func NewGenOther(id int) *genOther {
	return &genOther{
		start: time.Now(),
		data: &model.Enter{
			TS:       time.Now(),
			ID:       id + 1,
			Status:   false,
			LastOpen: time.Now().Add(-time.Hour * 2),
			Type:     "door",
		},
	}
}

func (gn *genOther) Scrape() ([]byte, error) {
	gn.data.Close()
	return json.Marshal(gn.data)
}
