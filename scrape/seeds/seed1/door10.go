package seed1

import (
	"encoding/json"
	"enter-scraper/model"
	"time"
)

type genDoor10 struct {
	start time.Time
	data  *model.Enter
}

func NewGenDoor10() *genDoor10 {
	return &genDoor10{
		start: time.Now(),
		data: &model.Enter{
			TS:       time.Now(),
			ID:       10,
			Status:   false,
			LastOpen: time.Now().Add(-time.Hour * 2),
			Type:     "door",
		},
	}
}

func (gn *genDoor10) Scrape() ([]byte, error) {
	if sc := time.Since(gn.start); sc >= time.Second*1080 && sc < time.Second*1081 || sc >= time.Second*1740 && sc < time.Second*1741 {
		gn.data.Open()
		return json.Marshal(gn.data)
	}
	gn.data.Close()
	return json.Marshal(gn.data)
}
