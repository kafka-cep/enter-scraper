package scrape

import (
	"context"
	"enter-scraper/config"
	"fmt"
	"go.uber.org/zap"
	"sync"
	"time"
)

type ScrapeScheduler struct {
	logger *zap.Logger

	scrapers []Scraper
}

func NewScrapeScheduler(logger *zap.Logger, config config.ScrapeConfig) *ScrapeScheduler {
	scrapers := make([]Scraper, len(config.IPs))

	if config.Gen != 0 {
		scrapers = GenScrape(config.Gen)
	} else {
		scrapers = ScrapeFill(logger, config)
	}

	return &ScrapeScheduler{
		logger:   logger,
		scrapers: scrapers,
	}
}

// TODO: Подумать как сделать так, чтобы одновременно вызов
// TODO: Вынести тайм скрейпов в конфиг
func (sc *ScrapeScheduler) Schedule(ctx context.Context, send chan<- []byte, workCnt int) {
	/*
		go func() {
			defer close(send)

			for {
				select {
				case <-ctx.Done():
					return
				default:
					for _, val := range sc.scrapers {
						go func(scrape Scraper) {
							res, err := scrape.Scrape()
							if err != nil {
								sc.logger.Error(fmt.Sprintf("Cannot scrape data: %v", err))
							}
							send <- res
						}(val)
					}
				}
				time.Sleep(time.Second * 2)
			}
		}()*/
	scrapeCh := make(chan Scraper)

	go func() {
		defer close(scrapeCh)

		for {
			select {
			case <-ctx.Done():
				return
			default:
				for _, scrape := range sc.scrapers {
					scrapeCh <- scrape
				}
			}
			time.Sleep(time.Second * 1)
		}
	}()

	go func() {
		wg := sync.WaitGroup{}

		for i := 0; i < workCnt; i++ {
			wg.Add(1)

			go func() {
				defer wg.Done()
				for val := range scrapeCh {
					res, err := val.Scrape()
					if err != nil {
						sc.logger.Error(fmt.Sprintf("Cannot scrape data: %v", err))
					}
					send <- res
				}
			}()
		}

		wg.Wait()
	}()
}
