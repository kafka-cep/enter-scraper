package scrape

import (
	"fmt"
	"go.uber.org/zap"
	"io"
	"net/http"
	"net/url"
)

type SensorScrape struct {
	logger *zap.Logger
	url    *url.URL
}

func NewSensorScrape(logger *zap.Logger, rawUrl string) (*SensorScrape, error) {
	Url, err := url.Parse(rawUrl)
	if err != nil {
		return nil, err
	}
	return &SensorScrape{
		logger: logger,
		url:    Url,
	}, nil

}

// TODO: Можно вынести ошибки в другую структуру
// TODO: Возможно лучше передать ошибку выше и там залогировать
func (sc *SensorScrape) Scrape() ([]byte, error) {
	resp, err := http.Get(sc.url.String())
	if err != nil {
		sc.logger.Error(fmt.Sprintf("Cannot get data from sensor %v", sc.url.String()))
		return nil, err
	}
	defer resp.Body.Close()

	//TODO: Возможно вынести проверку запроса в отдельный метод
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		err = fmt.Errorf("Cannot get data from sensor %v. Response status code: %v", sc.url.String(), resp.StatusCode)
		sc.logger.Error(err.Error())
		return nil, err
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		err = fmt.Errorf("Cannot read data from sensor %v", sc.url.String())
		sc.logger.Error(err.Error())
		return nil, err
	}

	return body, nil
}
