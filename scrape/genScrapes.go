package scrape

import "enter-scraper/scrape/seeds/seed1"

// TODO: Придумать как вынести в интерфейс
func GenScrapes() []Scraper {
	res := make([]Scraper, 21)

	for i := range res {
		res[i] = seed1.NewGenOther(i)
	}

	res[0] = seed1.NewGenDoor1()
	res[2] = seed1.NewGenDoor3()
	res[4] = seed1.NewGenDoor5()
	res[6] = seed1.NewGenDoor7()
	res[9] = seed1.NewGenDoor10()
	res[14] = seed1.NewGenDoor15()

	return res
}
