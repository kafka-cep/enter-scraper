package controller

import (
	"context"
	"enter-scraper/config"
	"enter-scraper/scrape"
	"enter-scraper/sender"
	"fmt"
	"go.uber.org/zap"
	"sync"
)

type Controller struct {
	logger     *zap.Logger
	aggregConf config.AggregatorConfig
	scrapeConf config.ScrapeConfig
}

func NewController(logger *zap.Logger, aggreg config.AggregatorConfig, scrape config.ScrapeConfig) *Controller {
	cont := &Controller{
		logger:     logger,
		aggregConf: aggreg,
		scrapeConf: scrape,
	}

	return cont
}

// TODO: Надо добавить больше контроля за временем жизни горутин
func (cont Controller) Control() error {
	cont.logger.Info("Controller try start...")

	ch := make(chan []byte, 10)
	ctx, _ := context.WithCancel(context.Background())

	ag, err := sender.SetSender("aggregator", cont.logger, cont.aggregConf)
	if err != nil {
		cont.logger.Error(fmt.Sprintf("Cannot create sender with error: %v", err))
		return err
	}

	sched := scrape.NewScrapeScheduler(cont.logger, cont.scrapeConf)

	sched.Schedule(ctx, ch, 10)

	// TODO: Возможно нужна прослойка для Sender
	wg := sync.WaitGroup{}

	for i := 0; i < 10; i++ {
		wg.Add(1)

		go func() {
			defer wg.Done()
			for val := range ch {
				err = ag.Send(val)
				if err != nil {
					cont.logger.Error(fmt.Sprintf("Cannot send data: %v", err))
				}
			}
		}()
	}

	wg.Wait()
	return nil
}
